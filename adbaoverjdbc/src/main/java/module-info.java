/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.asyncjdbc.aoj.DataSourceFactory;

module org.asyncjdbc.adbaoverjdbc {
  requires jdk.incubator.adba;
  requires java.sql;
  requires java.logging;
  exports org.asyncjdbc.aoj;
  provides jdk.incubator.sql2.DataSourceFactory with DataSourceFactory;
}
