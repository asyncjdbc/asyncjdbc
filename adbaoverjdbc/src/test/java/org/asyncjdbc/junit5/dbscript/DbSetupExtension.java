/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.dbscript;

import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.hsqldb.cmdline.SqlFile;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * Extension to setup and tear down a database.
 */
public class DbSetupExtension implements BeforeEachCallback, AfterEachCallback {

  // TODO Change to ParameterResolver?

  private static final AtomicInteger dbCount = new AtomicInteger(0);

  public static final String DEFAULT_INIT_SCRIPT = "/sql/hsqldb/test_db.sql";
  private static final String DEFAULT_URL_BASE = "jdbc:hsqldb:mem:asyncjdbctest";
  private static final String DEFAULT_USER = "SA";
  private static final String DEFAULT_PASSWORD = "";

  private final List<ScriptResource> dbScriptResources;
  private Connection initialConnection;

  private DbSetupExtension(List<ScriptResource> dbScriptResources) {
    this.dbScriptResources = dbScriptResources;
  }

  /**
   * Creates the default database setup rule, initializing with the script
   * {@link #DEFAULT_INIT_SCRIPT}.
   *
   * @return Database setup rule
   */
  public static DbSetupExtension defaultDbSetup() {
    return builder()
        .addClassPathScript(DEFAULT_INIT_SCRIPT)
        .build();
  }

  /**
   * Creates an empty builder for a database setup rule.
   *
   * @return Empty builder
   */
  public static DbSetupExtension.Builder builder() {
    return new Builder();
  }

  public String getConnectionUrl() {
    return DEFAULT_URL_BASE + dbCount.get() + ";ifexists=true";
  }

  public String getUser() {
    return DEFAULT_USER;
  }

  public String getPassword() {
    return DEFAULT_PASSWORD;
  }

  @Override
  public void beforeEach(ExtensionContext context) throws Exception {
    if (initialConnection != null) {
      afterEach(context);
    }
    initialConnection = DriverManager
        .getConnection(getInitialConnectionString(), DEFAULT_USER, DEFAULT_PASSWORD);
    dbScriptResources.forEach(this::executeScript);
  }

  @Override
  public void afterEach(ExtensionContext context) throws SQLException {
    if (initialConnection == null) {
      return;
    }
    try (Connection local = initialConnection;
        Statement stmt = local.createStatement()) {
      // force shutdown
      stmt.execute("SHUTDOWN");
    } finally {
      initialConnection = null;
    }
  }

  private String getInitialConnectionString() {
    return DEFAULT_URL_BASE + dbCount.incrementAndGet() + ";create=true;shutdown=true";
  }

  private void executeScript(ScriptResource scriptResource) {
    try (Reader scriptReader = scriptResource.getScriptReader()) {
      SqlFile sqlFile = new SqlFile(scriptReader, scriptResource.getLabel(), null, "UTF-8", false,
          null);
      sqlFile.setConnection(initialConnection);
      sqlFile.execute();
    } catch (Exception e) {
      throw new ScriptException("Failure to execute script " + scriptResource.getLabel(), e);
    }
  }

  public static class Builder {

    private final List<ScriptResource> dbScriptResources = new ArrayList<>();

    /**
     * Adds a SQL script from the class path.
     *
     * @param resourceName Resource which can be loaded using {@code
     *     DbSetupRule.class.getResourceAsStream}
     * @return this builder
     */
    public Builder addClassPathScript(String resourceName) {
      return addScriptResource(new ClassPathScriptResource(DbSetupExtension.class, resourceName));
    }

    /**
     * Adds a string with SQL script.
     *
     * @param label Short label for the script (used for logging)
     * @param stringScript string with SQL script
     * @return this builder
     */
    public Builder addStringScript(String label, String stringScript) {
      return addScriptResource(new StringScriptResource(label, stringScript));
    }

    public Builder addScriptResource(ScriptResource scriptResource) {
      dbScriptResources.add(scriptResource);
      return this;
    }

    public DbSetupExtension build() {
      return new DbSetupExtension(dbScriptResources);
    }

  }

}
