/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.dbscript;

import static java.util.Objects.requireNonNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * Script resource for reading classpath resources.
 */
public final class ClassPathScriptResource implements ScriptResource {

  private final Class<?> resourceRoot;
  private final String resourceName;

  /**
   * Script resource for the {@code resourceName} class path resource.
   *
   * <p>The resource is resolved using {@code ClassPathScriptResource.class.getResourceAsStream}.
   * Use {@link #ClassPathScriptResource(Class, String)} to use a different class as root.
   *
   * @param resourceName Absolute or relative resource name
   */
  public ClassPathScriptResource(String resourceName) {
    this(ClassPathScriptResource.class, resourceName);
  }

  /**
   * Script resource for the {@code resourceName} class path resource.
   *
   * <p>The resource is resolved using {@code resourceRoot.getResourceAsStream}.
   *
   * @param resourceRoot Class to use as root for resolving resources
   * @param resourceName Absolute or relative resource name
   */
  public ClassPathScriptResource(Class<?> resourceRoot, String resourceName) {
    this.resourceRoot = requireNonNull(resourceRoot, "resourceRoot");
    this.resourceName = requireNonNull(resourceName, "resourceName");
  }

  @Override
  public Reader getScriptReader() throws IOException {
    InputStream resourceStream = resourceRoot.getResourceAsStream(resourceName);
    if (resourceStream == null) {
      throw new FileNotFoundException("Could not find class path resource " + resourceName);
    }
    return new InputStreamReader(resourceStream, StandardCharsets.UTF_8);
  }

  @Override
  public String getLabel() {
    return resourceName;
  }

}
