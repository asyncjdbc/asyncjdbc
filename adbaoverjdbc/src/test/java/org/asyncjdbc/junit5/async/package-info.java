/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * JUnit 5 extensions for asynchronous testing.
 *
 * <p>TODO: Consider spinning this off to its own library when it is more stable/feature-rich
 *
 * <p>Credits: we have been inspired by (read: based this on) the Vert.X JUnit 5 extensions in
 * <a href="https://github.com/vert-x3/vertx-junit5">https://github.com/vert-x3/vertx-junit5</a>,
 * and by <a href="https://developers.redhat.com/blog/2018/01/23/vertx-junit5-async-testing/">this
 * article</a>.
 */

package org.asyncjdbc.junit5.async;