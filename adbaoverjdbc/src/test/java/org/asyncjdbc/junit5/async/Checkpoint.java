/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Portions Copyright (c) 2018 Red Hat, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.async;

/**
 * A test completion checkpoint, flagging it advances towards the test context completion.
 *
 * <p>Parts of this implementation have been derived from
 * <a href="https://github.com/vert-x3/vertx-junit5">vertx-junit5</a>
 *
 * @see AsyncTestContext
 */
public interface Checkpoint {

  /**
   * Name for the checkpoint.
   *
   * @return name
   */
  String name();

  /**
   * Flags the checkpoint.
   */
  void flag();

}
