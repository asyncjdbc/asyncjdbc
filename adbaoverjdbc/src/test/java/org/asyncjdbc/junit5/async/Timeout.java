/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Red Hat, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.junit5.async;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * Specify how long {@link AsyncTestContext#awaitCompletion(long, TimeUnit)} waits before timing
 * out.
 *
 * <p>This annotation works on both test methods and test classes.
 *
 * <p>Parts of this implementation have been derived from
 * <a href="https://github.com/vert-x3/vertx-junit5">vertx-junit5</a>
 *
 * @see org.asyncjdbc.junit5.async.AsyncTestContext
 * @see org.junit.jupiter.api.Test
 * @see org.junit.jupiter.api.extension.ExtendWith
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Timeout {

  /**
   * Timeout value.
   *
   * @return Timeout
   */
  int value();

  /**
   * Time unit of timeout.
   *
   * @return Time unit
   */
  TimeUnit timeUnit() default TimeUnit.MILLISECONDS;

}
