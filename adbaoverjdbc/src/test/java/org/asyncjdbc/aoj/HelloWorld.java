/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.aoj;

import static org.junit.jupiter.api.Assertions.assertEquals;

import jdk.incubator.sql2.Connection;
import jdk.incubator.sql2.DataSource;
import jdk.incubator.sql2.DataSourceFactory;
import org.asyncjdbc.junit5.async.AsyncTestContext;
import org.asyncjdbc.junit5.async.AsyncTestExtension;
import org.asyncjdbc.junit5.async.Checkpoint;
import org.asyncjdbc.junit5.dbscript.DbSetupExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;

@ExtendWith(AsyncTestExtension.class)
public class HelloWorld {

  @RegisterExtension
  public final DbSetupExtension dbSetupExtension = DbSetupExtension.defaultDbSetup();

  private static final String FACTORY_NAME = "org.asyncjdbc.aoj.DataSourceFactory";

  @Test
  public void test(AsyncTestContext context) {
    DataSourceFactory factory = DataSourceFactory.newFactory(FACTORY_NAME);
    if (factory == null) {
      context.failNow("Error: Could not get a DataSourceFactory!");
    } else {
      System.out.println("DataSourceFactory: " + factory);
      try (DataSource ds = factory.builder()
          .url(dbSetupExtension.getConnectionUrl())
          .username(dbSetupExtension.getUser())
          .password(dbSetupExtension.getPassword())
          .build();
          Connection conn = ds.getConnection(context::failNow)) {
        System.out.println("Connected! DataSource: " + ds + " Connection: " + conn);

        Checkpoint validationCheckpoint = context.checkpoint("validation");
        conn.rowOperation("SELECT 1 from (values (1))")
            .collect(() -> null,
                (a, r) -> {
                  context.verify(() ->
                      assertEquals(Integer.valueOf(1), r.at(1).get(Integer.class)));
                  validationCheckpoint.flag();
                })
            .onError(context::failNow)
            .submit();
      }
    }
  }
}
