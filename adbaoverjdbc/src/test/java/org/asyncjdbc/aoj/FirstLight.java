/*
 * Portions Copyright 2018 asyncjdbc.org and/or its affiliates
 * Portions Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.asyncjdbc.aoj;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.concurrent.CompletionStage;
import java.util.stream.Collector;
import jdk.incubator.sql2.AdbaType;
import jdk.incubator.sql2.Connection;
import jdk.incubator.sql2.DataSource;
import jdk.incubator.sql2.DataSourceFactory;
import jdk.incubator.sql2.Result;
import jdk.incubator.sql2.Transaction;
import org.asyncjdbc.junit5.async.AsyncTestContext;
import org.asyncjdbc.junit5.async.AsyncTestExtension;
import org.asyncjdbc.junit5.async.Checkpoint;
import org.asyncjdbc.junit5.dbscript.DbSetupExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;

/**
 * This is a quick and dirty test to check if anything at all is working.
 *
 * <p>Depends on the scott/tiger schema available here:
 * https://github.com/oracle/dotnet-db-samples/blob/master/schemas/scott.sql
 */
@ExtendWith(AsyncTestExtension.class)
public class FirstLight {

  @RegisterExtension
  public final DbSetupExtension dbSetupExtension = DbSetupExtension.defaultDbSetup();

  // Define this to be the most trivial SELECT possible
  private static final String TRIVIAL = "SELECT 1 FROM (values (1))";
  private static final String FACTORY_NAME = "org.asyncjdbc.aoj.DataSourceFactory";

  /**
   * Verify that DataSourceFactory.forName works. Can't do anything without that.
   */
  // TODO Move test; doesn't need DB setup
  @Test
  void firstLight() {
    assertEquals("org.asyncjdbc.aoj.DataSourceFactory",
        DataSourceFactory.newFactory(FACTORY_NAME).getClass().getName());
  }

  /**
   * Verify that can create a DataSource, though not a Connection. Should work
   * even if there is no database.
   */
  // TODO Move test; doesn't need DB setup
  @Test
  void createDataSource() {
    DataSourceFactory factory = DataSourceFactory.newFactory(FACTORY_NAME);
    DataSource ds = factory.builder()
        .url(dbSetupExtension.getConnectionUrl())
        .username(dbSetupExtension.getUser())
        .password(dbSetupExtension.getPassword())
        .build();
    assertNotNull(ds);
  }

  /**
   * create a Connection and send a SQL to the database.
   */
  @Test
  void sqlOperation(AsyncTestContext context) {
    Checkpoint validationCheckpoint = context.checkpoint("validation");

    DataSourceFactory factory = DataSourceFactory.newFactory(FACTORY_NAME);
    DataSource ds = factory.builder()
        .url(dbSetupExtension.getConnectionUrl())
        .username(dbSetupExtension.getUser())
        .password(dbSetupExtension.getPassword())
        .build();
    
    try (Connection conn = ds.getConnection(context::failNow)) {
      assertNotNull(conn);
      conn.operation(TRIVIAL)
          .onError(context::failNow)
          .submit()
          .getCompletionStage()
          .thenRun(validationCheckpoint::flag);
    }
  }

  /**
   * Execute a few trivial queries.
   */
  @Test
  void rowOperation(AsyncTestContext context) {
    DataSourceFactory factory = DataSourceFactory.newFactory(FACTORY_NAME);
    try (DataSource ds = factory.builder()
        .url(dbSetupExtension.getConnectionUrl())
        .username(dbSetupExtension.getUser())
        .password(dbSetupExtension.getPassword())
        .build();
        Connection conn = ds.getConnection(context::failNow)) {
      assertNotNull(conn);

      Checkpoint trivialResultCheckpoint = context.checkpoint("trivialResult");
      conn.<Void>rowOperation(TRIVIAL)
          .collect(() -> null,
              (a, r) -> {
                context.verify(() ->
                    assertEquals(Integer.valueOf(1), r.at(1).get(Integer.class), "trivial result"));
                trivialResultCheckpoint.flag();
              })
          .submit();

      Checkpoint laborCostCheckpoint = context.checkpoint("laborCost");
      conn.<Integer>rowOperation("select * from emp")
          .onError(context::failNow)
          .collect(Collector.of(() -> new int[1],
              (int[] a, Result.RowColumn r) ->
                  a[0] = a[0] + r.at("SAL").get(Integer.class),
              (l, r) -> l,
              a -> a[0]))
          .submit()
          .getCompletionStage()
          .thenAccept(n -> {
            context.verify(() ->
                assertEquals(Integer.valueOf(29025), n, "labor cost"));
            laborCostCheckpoint.flag();
          });

      Checkpoint employeeSalaryCheckpoint = context.checkpoint("employeeSalary");
      conn.<Integer>rowOperation("select * from emp where empno = ?")
          .set("1", 7782)
          .onError(context::failNow)
          .collect(() -> null,
              (a, r) -> {
                context.verify(() ->
                    assertEquals(Integer.valueOf(2450), r.at("SAL").get(Integer.class),
                        "employee salary"));
                employeeSalaryCheckpoint.flag();
              })
          .submit();
    }
  }

  /**
   * check does error handling do anything.
   */
  @Test
  void errorHandling(AsyncTestContext context) {
    DataSourceFactory factory = DataSourceFactory.newFactory(FACTORY_NAME);

    Checkpoint exceptionOnConnect = context.checkpoint("exceptionOnConnect");
    try (DataSource ds = factory.builder()
        .url(dbSetupExtension.getUser()) // TODO Intentional error (user instead of url)?
        .username(dbSetupExtension.getPassword())
        .password("invalid password")
        .build();
        Connection conn = ds.getConnection(t -> exceptionOnConnect.flag())) {

      conn.<Void>rowOperation(TRIVIAL)
          .collect(() -> null,
              (a, r) -> context.failNow("Should not be called for trivial operation"))
          .onError(t -> System.out.println(t.toString()))
          .submit();

      conn.catchErrors();
    }

    try (DataSource ds = factory.builder()
        .url(dbSetupExtension.getConnectionUrl())
        .username(dbSetupExtension.getUser())
        .password(dbSetupExtension.getPassword())
        .build();
        Connection conn = ds.getConnection(context::failNow)) {
      Checkpoint employeeSalaryCheckpoint = context.checkpoint("employeeSalary");
      conn.<Integer>rowOperation("select * from emp where empno = ?")
          .set("1", 7782)
          .collect(Collector.of(
              () -> null,
              (a, r) -> {
                context.verify(() ->
                    assertEquals(Integer.valueOf(2450), r.at("SAL").get(Integer.class),
                        "employee salary"));
                employeeSalaryCheckpoint.flag();
              },
              (l, r) -> null))
          .onError(context::failNow)
          .submit();
    }
  }

  /**
   * Do something that approximates real work. Do a transaction. Uses
   * Transaction, CompletionStage args, and catch Operation.
   */
  @Test
  void transaction(AsyncTestContext context) {
    DataSourceFactory factory = DataSourceFactory.newFactory(FACTORY_NAME);
    try (DataSource ds = factory.builder()
        .url(dbSetupExtension.getConnectionUrl())
        .username(dbSetupExtension.getUser())
        .password(dbSetupExtension.getPassword())
        .build();
        Connection conn = ds.getConnection(context::failNow)) {

      Checkpoint validationCheckpoints = context.checkpoint("validation", 2);
      Transaction trans = conn.transaction();
      CompletionStage<Integer> idF = conn.<Integer>rowOperation(
          "select empno, ename from emp where ename = ? for update")
          .set("1", "CLARK", AdbaType.VARCHAR)
          .collect(Collector.of(
              () -> new int[1],
              (a, r) -> a[0] = r.at("EMPNO").get(Integer.class),
              (l, r) -> null,
              a -> a[0])
          )
          .submit()
          .getCompletionStage();

      idF.thenAccept(id -> {
        context.verify(() ->
            assertEquals(Integer.valueOf(7782), id, "empno"));
        validationCheckpoints.flag();
      });

      conn.<Long>rowCountOperation("update emp set deptno = ? where empno = ?")
          .set("1", 40, AdbaType.INTEGER)
          .set("2", idF, AdbaType.INTEGER)
          .apply(c -> {
            if (c.getCount() != 1L) {
              trans.setRollbackOnly();
              throw new IllegalStateException("updated wrong number of rows");
            }
            return c.getCount();
          })
          .onError(context::failNow)
          .submit()
          .getCompletionStage()
          .thenAccept(c -> {
            context.verify(() ->
                assertEquals(Long.valueOf(1), c, "record count"));
            validationCheckpoints.flag();
          });

      conn.catchErrors();
      conn.commitMaybeRollback(trans);
    }
  }
}
